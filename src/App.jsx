import React from 'react'
import { useState } from 'react'
import './App.scss';
import ModalImage from './components/ModalImage/ModalImage'
import ModalText from './components/ModalText/ModalText'
import ShopPage from './components/ShopPage/ShopPage';
import Header from './components/Header/Header';
import MainPage from './components/MainPage/MainPage';

function App() {
  
  const [modalToShow, setModalToShow] = useState(false);
  const [modalActive, setModalActive] = useState(false);
  const [productName, setProductName] = useState('');
  const [productDescription, setProductDescription] = useState('');
  const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || [])
  const [star, setStar] = useState(JSON.parse(localStorage.getItem('star')) || [])

  function showDeleteModal() {
      setModalToShow(true)
  }

  function closeDeleteModal() {
      setModalToShow(false)
  }

  function showAddModal(productName, productDescription) {
    setModalActive(true);
    setProductName(productName);
    setProductDescription(productDescription);
  }

  function closeAddModal() {
        setModalActive(false)
  }
  
  function addCartToLocalStorage(id) {
    const cart = JSON.parse(localStorage.getItem('cart')) || []
    if (!cart.includes(id)) {
      const newState = [...cart, id]
      localStorage.setItem('cart', JSON.stringify(newState))
      setCart(newState);
    }
  }

  function addStarToLocalStorage(id) {
    const star = JSON.parse(localStorage.getItem('star')) || []
    if (!star.includes(id)) {
      const newStarState = [...star, id]
      localStorage.setItem('star', JSON.stringify(newStarState))
      setStar(newStarState);
    }
  }

  
  return (
    <>
      <Header cart={cart} star={star} />
      <MainPage/>
      <ShopPage showAddModal={showAddModal} addCartToLocalStorage={addCartToLocalStorage } addStarToLocalStorage={addStarToLocalStorage} />
      <ModalImage
        active={modalToShow} 
        setActive={closeDeleteModal} 
        firstText={"NO, CANCEL"} 
        secondaryText={'YES, DELETE'} 
        firstClick={closeDeleteModal} 
        secondaryClick={closeDeleteModal}  
      />
      <ModalText
        active={modalActive} 
        setActive={closeAddModal} 
        firstText={'ADD TO CART'} 
        isSecondModal={true} 
        firstClick={closeAddModal} 
        productName={productName}
        productDescription={productDescription}
      />

    </>
  );
}

export default App;
