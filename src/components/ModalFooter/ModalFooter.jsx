import Button from '../Button/Button'
import './ModalFooter.scss'
import '../Button/Button.scss'

function ModalFooter({ firstText, secondaryText, firstClick, secondaryClick, isSecondModal }) {
    
    return (
        <>
            <div className={`modal-footer ${isSecondModal ? 'second-modal-footer' : ''}`}>
                {firstText && <Button className='btn-violet' onClick={firstClick}>{firstText}</Button>}
                {secondaryText && <Button onClick={secondaryClick}>{secondaryText}</Button>}
            </div>
        </>
    )
}

export default ModalFooter;
