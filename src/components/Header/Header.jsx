import logoImage from '../../assets/sinsay-logo.png'
import cartIcon from '../../assets/cart-icon.png'
import starIcon from '../../assets/star-icon.svg'
import './Header.scss'


function Header({ cart, star }) {

    return (
        <>
            <nav className='header-nav'>
                <img src={logoImage} width='150' alt="" />
                <ul className='header-nav__menu'>
                    <li className='header-nav__item'><a href="">Жінка</a></li>
                    <li className='header-nav__item'><a href="">Чоловік</a></li>
                    <li className='header-nav__item'><a href="">Дитина</a></li>
                    <li className='header-nav__item'><a href="">Немовля</a></li>
                    <li className='header-nav__item'><a href="">Дім</a></li>
                </ul>
                <ul className='header-nav__icons-box'>
                    <li className="star">
                        <img className="star__icon" width='30' src={starIcon} alt="starIcon" />
                        <span>{star.length}</span>
                    </li>
                    <li className="cart">
                        <img className="cart__icon" width='30' src={cartIcon} alt="cartIcon" />
                        <span>{cart.length}</span>
                    </li>
                </ul>
            </nav>
        </>
    )
}

export default Header;