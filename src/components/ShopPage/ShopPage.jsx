import { useState, useEffect } from "react";
import ProductCard from "../ProductCard/ProductCard";
import './ShopPage.scss';


function ShopPage({showAddModal, addCartToLocalStorage, addStarToLocalStorage}) {

    const [items, setItems] = useState([]);

    useEffect(() => {
        fetch('products.json')
            .then(response => response.json())
            .then(data => setItems(data));
    }, [])

    return (
        <>
            <div className="shop">
                {items.map((product) =>
                    <ProductCard
                        imageUrl={product.imageUrl}
                        name={product.name}
                        sku={product.sku}
                        color={product.color}
                        price={product.price}
                        key={product.id}
                        id={product.id}
                        description={product.description}
                        showAddModal={showAddModal}
                        addCartToLocalStorage={addCartToLocalStorage}
                        addStarToLocalStorage={addStarToLocalStorage}
                    />
                )
                }
        </div>
        </>
    )  
}

export default ShopPage;