import './ModalClose.scss'

function ModalClose({onClick}) {
    return (
        <>
            <button className="modal-close" onClick={onClick}>
                &times;
            </button>
        </>
    )
}

export default ModalClose;