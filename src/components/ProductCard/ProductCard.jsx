import Button from '../Button/Button';
import './ProductCard.scss'
import { useState, useEffect } from 'react';
import StarIcon from '../StarIcon/StarIcon';



function ProductCard(props) {

    const [starClick, setStarClick] = useState(false);

    useEffect(() => {
        const storedState = JSON.parse(localStorage.getItem(`starClick-${props.id}`));
        if (storedState !== null) {
            setStarClick(storedState);
        }       
    }, [props.id]);

    function handleStarClick() {
        const newState = !starClick;
        setStarClick(newState);
        localStorage.setItem(`starClick-${props.id}`, JSON.stringify(newState));
    };
    
        return (
            <div className="product">
                <a className='product__link' href="#">
                    <img className="product__image" src={props.imageUrl} alt=""/>
                    <div className="product__icon">
                        <StarIcon starClick={starClick} handleStarClick={(event) => { event.preventDefault(); handleStarClick(); props.addStarToLocalStorage(props.id) }} /> 
                    </div>
                    <div className='product__main-info'>
                        <h3 className="product__name">{props.name}</h3>
                    </div>
                    <div className='product__description'>
                        <div>
                            <p className="product__vendor-code">{props.sku}</p>
                            <p className="color">{props.color}</p>
                            <p className="product__price">{props.price} UAH</p>
                        </div>
                        <Button  onClick={(event) => {event.preventDefault(); props.addCartToLocalStorage(props.id); props.showAddModal(props.name, props.description); }} className='btn btn-card'>Add to cart</Button>
                    </div>
                </a>
            </div>
        )
    }

export default ProductCard;