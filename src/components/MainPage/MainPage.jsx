import bgImage from '../../assets/bg.avif'
import './MainPage.scss'


function MainPage() {
    return (
        <div>
            <img className='main__bg' src={bgImage} alt="" />
            <h3 className='main__text'>НОВИНКИ</h3>
        </div>
    )
}

export default MainPage;
