import './ModalWrapper.scss'

function ModalWrapper({active, setActive,children}) {
    return (
        <>
            <div className={active? 'modal-wrapper active':'modal-wrapper'} onClick = {()=>setActive(false)}>
                <div className={active ? 'modal-wrapper__content active' : 'modal-wrapper__content'} onClick={e => e.stopPropagation()}>
                    {children}
                </div> 
            </div>    
        </>
    )
}

export default ModalWrapper;