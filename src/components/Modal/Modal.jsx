import React from "react";
import ModalWrapper from "../ModalWrapper/ModalWrapper";
import ModalHeader from "../ModalHeader/ModalHeader";
import ModalClose from '../ModalClose/ModalClose'
import ModalBody from '../ModalBody/ModalBody'
import ModalFooter from '../ModalFooter/ModalFooter'


function Modal({active, setActive, children, firstText, secondaryText, firstClick, secondaryClick, isSecondModal}){
    return (
        <ModalWrapper active={active} setActive={setActive}>
            <ModalHeader >
                <ModalClose onClick={() => setActive(false)}/>
            </ModalHeader>
            <ModalBody>
                {children}
            </ModalBody>
            <ModalFooter  firstText={firstText} secondaryText={secondaryText} firstClick={firstClick} secondaryClick={secondaryClick} isSecondModal={isSecondModal}/>
        </ModalWrapper>
                
    );
};

export default Modal;